require("tidyverse")
require("phonTools")

allWords <- read.csv(file = "Data/forms.csv")
allLanguages <- read.csv(file = "Data/languages.csv")
referenceToConcepticon <- read.csv(file = "Data/parameters.csv")
concepticonFields <- read.csv(file = "Concepticon/semanticFields.csv")

noPhonemic <- filter(allWords, transcription != "Phonemic" & transcription != "phonetic" & alt_transcription != "Phonemic" & alt_transcription != "Phonemic (vars)")
languagesWithNoPhonemic <- factor(noPhonemic$Language_ID) %>% 
  levels() %>% 
  as.integer()
languagesWithNoPhonemicNames <- allLanguages$Name[match(languagesWithNoPhonemic, allLanguages$ID)] %>% 
  as.tibble() %>% 
  dplyr::arrange()

completeWords <- as.tibble(allWords) %>%
  select(., ID, Language_ID, Parameter_ID, Form, transcription, alt_form, alt_transcription) %>% 
  mutate(., concepticonID = referenceToConcepticon$Concepticon_ID[match(.$Parameter_ID, referenceToConcepticon$ID)]) %>% 
  mutate(., englishName = referenceToConcepticon$Name[match(.$Parameter_ID, referenceToConcepticon$ID)]) %>%
  mutate(., language = allLanguages$Name[match(.$Language_ID, allLanguages$ID)])

completeWords <- mutate(completeWords, semanticField = concepticonFields$semanticfield[match(completeWords$concepticonID, concepticonFields$id)]) %>% 
  mutate(., ontologicalCategory = concepticonFields$ontological_category[match(.$concepticonID, concepticonFields$id)]) %>% 
  select(Form, englishName, language, semanticField, everything())

proportionOfSemantics <- select(completeWords, semanticField) %>% 
  table() %>%
  as.tibble() %>% 
  mutate(., proportion = (n / sum(n)) * 100) %>% 
  arrange(., desc(proportion)) %>%
  setNames(., c("semanticField", "sroportion", "percentage")) %>% 
  select(., semanticField, percentage)

proportionOfOntology <- select(completeWords, ontologicalCategory) %>% 
  table() %>%
  as.tibble() %>% 
  mutate(., proportion = (n / sum(n)) * 100) %>% 
  arrange(., desc(proportion)) %>%
  setNames(., c("ontologicalCategory", "sroportion", "percentage")) %>% 
  select(., ontologicalCategory, percentage)


# Add info from other databases

kupermanAgeOfAcquisition <- read_tsv(file = "Data/Linked concepticons/aoa_kuperman.tsv")
psycholinguisticsMRC <- read_tsv(file = "Data/Linked concepticons/mrc.tsv")
wordNet <- read_tsv(file = "Data/Linked concepticons/wordnet.tsv")

wordsAndData <- mutate(completeWords, typeOfWord = wordNet$WORDNET_POS[match(completeWords$concepticonID, wordNet$CONCEPTICON_ID)]) %>% 
  mutate(., ageOfAcquisitionMeanKup = kupermanAgeOfAcquisition$RATING_MEAN[match(completeWords$concepticonID, kupermanAgeOfAcquisition$CONCEPTICON_ID)]) %>% 
  mutate(., ageOfAcquisitionSDKup = kupermanAgeOfAcquisition$RATING_SD[match(completeWords$concepticonID, kupermanAgeOfAcquisition$CONCEPTICON_ID)])

allCharacters <- completeWords$Form %>%
  str_split(., "", simplify = T) %>% 
  str_replace_all(., " ", "") %>% 
  unique()

#matriz con todas las palabras como vectores de caracteres
#esta matriz se puede usar para generar vectores booleanos que dicen con cuál matchean de allCharacters
#ejemplo con 1 fila:
"%in%"(allCharacters, matrixOfWords[1,])
#con Apply se puede hacer una matriz
x <- apply(X = matrixOfWords, MARGIN = 1, FUN = function(x, y){
  prelim <- "%in%"(y, x)
  return(as.integer(prelim))
}, y = allCharacters) %>% 
  t() %>% 
  set_colnames(allCharacters) %>% 
  set_rownames(completeWords$Form)